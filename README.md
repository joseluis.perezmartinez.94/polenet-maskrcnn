# INSTALACIÓN

## 1. Instalar Docker

En este documento se recoge la instalación de Docker Engine para Ubuntu desde el repositorio oficial. Esta misma guía y otros métodos de instalación se pueden consultar en la [página web oficial](https://docs.docker.com/engine/install/ubuntu/). Para instalar Docker en otra distribución consultar [aquí](https://docs.docker.com/engine/install/).

### 1.1 Preparar el repositorio

1) Actualizar `apt`:

``` sh
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

2) Añadir la clave GPG de Docker:

``` sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

3) Configurar el repositorio estable:

``` sh
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Para ver otras opciones de otros repositorios, consultar la [página oficial](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).

### 1.2 Instalar Docker Engine

1) Actualizar el índice de paquetes de `apt` e instalar la última versión de Docker Engine:

``` sh
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Para instalar una versión específica de Docker Engine consultar la [página oficial](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).

2) Verificar que Docker Engine se ha instalado correctamente:

``` sh
sudo docker run hello-world
```

Este comando descarga una imagen de prueba y ejecuta el contenedor. Al ejecutarse, muestra el mensaje y finaliza.

3) Docker Engine ya está instalado. Se ha creado el grupo `docker`, pero no hay usuarios añadidos a él. Para ejecutar comandos Docker hay que usar `sudo` o añadir usuarios a ese grupo. Para realizar esta y otras configuraciones post-instalación, consultar [aquí](https://docs.docker.com/engine/install/linux-postinstall/).

## 2. Instalar Nvidia-Docker

Nvidia-Docker contiene los packetes de drivers, incluyendo CUDA, necesarios para poder utilizar la tarjeta gráfica desde Docker. Se va a cubrir la instalación para Ubuntu siguiendo las instrucciones de la [página oficial](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker) para Ubuntu.

### 2.1 Configurar Docker

``` sh
curl https://get.docker.com | sh \
  && sudo systemctl --now enable docker
```

### 2.2 Configurar el contenedor del Toolkit de NVIDIA

1) Configurar el repositorio estable y la clave GPG:

``` sh
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
```

Para ver otras opciones de otros repositorios, consultar la [página oficial](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#setting-up-nvidia-container-toolkit).

2) Instalar el paquete `nvidia-docker2` (y sus dependencias):

``` sh
sudo apt-get update
sudo apt-get install -y nvidia-docker2
```

3) Reiniciar el daemon de Docker para completar la instalación:

``` sh
sudo systemctl restart docker
```

4) Comprobar que la instalación se ha completado con éxito:

``` sh
sudo docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
``` 

Si todo ha ido bien, se debería mostrar una salida similar a la siguiente:

``` sh
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.51.06    Driver Version: 450.51.06    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla T4            On   | 00000000:00:1E.0 Off |                    0 |
| N/A   34C    P8     9W /  70W |      0MiB / 15109MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

## 3. Carpeta del proyecto

**WIP**

En el equipo se espera tener una carpeta base del proyecto a la que nos referiremos como `PROJ`. En esta carpeta se espera encontrar la carpeta `Tests`, con los distintos datasets sobre los que se va a trabajar. También se recomienda tener una carpeta `results` en la que colocar los resultados que se quieras extraer del contenedor Docker de YOLO5.

```
PROJ
 |
 - Tests
    |
    - 22009 Azh
    - Azahar 21334
    - Azahar_21181
    - ...
 - results
    |
    - ...
```

En este apartado tomaremos la carpeta PROJ como directorio raíz y trabajaremos con rutas relativas a éste.

### 3.1 Clonar el repositorio `polenet-maskrcnn`

Todos los ficheros desarrollados para trabajar con el dataset de Polenet con Mask-RCNN se encuentran en el repositorio de GITLAB [`polenet-maskrcnn`](https://gitlab.com/joseluis.perezmartinez.94/polenet-maskrcnn).

Para trabajar con el repositorio, primero hay que instalar GIT. Esto se puede hacer con el comando:

``` sh
sudo apt-get install git-all
```

Una vez tenemos instalado GIT, podemos clonar el repositorio ejecutando desde PROJ el comando:

``` sh
git clone https://gitlab.com/joseluis.perezmartinez.94/polenet-maskrcnn.git
cd polenet-maskrcnn
```

**Nota**: Este comando solicitará las credenciales de la cuenta de GITLAB. Se puede crear una cuenta gratuitamente en la [página web](https://gitlab.com/users/sign_in). Adicionalmente, puede que este paso falle si no se tienen los permisos necesarios. Si esto ocurre, se puede contactar conmigo (<jopem13@etsid.upv.es>) para que revise y actualice los permisos.

Tras clonar el repositorio, el directorio incluirá ahora la carpeta `polenet-maskrcnn`:

```
PROJ
 |
 - Tests
    |
    - 22009 Azh
    - Azahar 21334
    - Azahar_21181
    - ...
 - results
    |
    - ...
 - polenet-maskrcnn
    |
    - Mask_RCNN
       |
       - LICENSE
       - mrcnn
          |
          - ...
    - polenet
       |
       - polenet.py
       - confmat.py
       - ...
    - README.md
    - requirements.txt
    - Dockerfile
```

### 3.2 Preparar la imagen y el contenedor Docker

**TODO**

Este repositorio incluye un fichero Dockerfile que permite generar la imagen Docker con todo lo necesario para ejecutar Mask-RCNN. Esta imagen está basada en el Docker de Nvidia para [TensorFlow](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorflow) `tensorflow:22.01-tf1-py3` y depende de [Nvidia-Docker](#2-instalar-nvidia-docker). Para preparar el contenedor Docker hay que seguir los siguientes pasos:

1. Crear la imagen Docker:

En el directorio base del repositorio se encuentra el fichero [`Dockerfile`](https://gitlab.com/joseluis.perezmartinez.94/polenet-maskrcnn/-/blob/master/Dockerfile) con las instrucciones para crear la imagen Docker para Mask-RCNN. Para crear la imagen se usa el comando docker `build`.

```sh 
sudo docker build -t mask-rcnn:tf1-py3 .
```

La primera vez que se ejecute el comando se descargará la imagen Docker de TensorFlow, que sirve de base para nuestra imagen. El proceso de descarga es automático, pero puede tardar un rato ya que la imagen Docker de TensorFlow pesa alrededor de 15GB. Una vez terminado el proceso de crear nuestra imagen Docker para Mask-RCNN, ya no es necesario conservar la original de TensorFlow, así que se puede eliminar mediante:

```sh
sudo docker rmi <docker_image_id>
```

Se puede consultar el ID de las imágenes Docker descargadas en el equipo con el comando `docker images`.

De todas formas, en un principio es recomendable conservar la imagen base de TensorFlow para no tener que volver a descargarla en caso de que se necesite volver a construir nuestra imagen.

2. Crear el contenedor Docker:

Una vez completado el proceso de creación de la imagen Docker, pasamos a crear el contenedor.

```sh
sudo docker run --ipc=host -it -v /path/to/project/PROJ:/usr/src/app/PROJ --gpus all mask-rcnn:tf1-py3
```

Con el flag `-v path/to/host/folder:path/to/docker/folder` se pueden compartir carpetas del equipo con el contenedor Docker. En este ejemplo compartimos la carpeta raíz `PROJ`, pero como mínimo será necesario compartir los directorios con los datasets y una carpeta en la que exportar los resultados generados.

El flag `-it` inicia inmediatamente la sesión interactiva del contenedor Docker. Para salir del contenedor de puede usar el comando `exit` o la combinación `Ctrl + D`. Para volver a acceder al contenedor detenido, se puede usar el comando:

```sh
sudo docker start -i <container_id>
```

Donde el `<container_id>` se puede consultar el la lista de contenedores mediante:

```sh
sudo docker ps -a
```

## 4. Usar Mask-RCNN para Polenet

**TODO**

Dentro del contenedor Docker de Mask-RCNN, nos encontraremos en la ruta de trabajo por defecto `/usr/src/app` y tendremos la siguiente estructura de directorios:

```
app
 |
 - Mask_RCNN
    |
    - LICENSE
    - mrcnn
       |
       - ...
 - polenet
    |
    - polenet.py
    - confmat.py
    - ...
 - PROJ
    |
    - ...
```

En las carpetas `Mask_RCNN` y `polenet` tendremos el mismo contenido que en las del mismo nombres descargadas del repositorio, pero son copias locales. Los cambios en estos directorios no afectan fuera del contenedor Docker.

Por su parte, la carpeta `PROJ` es una copia virtual de la versión de nuestro equipo. Las modificaciones en este directorio se verán reflejadas en el host.

Para ejecutar Mask-RCNN para Polenet nos moveremos al directorio `polenet`:

```sh
cd polenet
```

La gestión de Mask-RCNN se hace a través del script [`polenet.py`](https://gitlab.com/joseluis.perezmartinez.94/polenet-maskrcnn/-/blob/master/polenet/polenet.py). Este script permite realizar las siguientes acciones sobre el dataset de Polenet:

- [Mostrar](#41-mostrar-clases-del-dataset) un resumen sobre las clases del dataset
- [Visualizar](#42-visualizar-im%C3%A1genes-del-dataset-con-etiquetas) las etiquetas de las clases sobre las imágenes
- [Entrenar](#43-entrenar-la-red) una red
- [Evaluar](#44-evaluar-la-red) la red entrenada
- [Detectar](#45-detectar-objetos) objetos con la red

Es posible usar `--help` o `-h` para obtener información adicional del uso de la aplicación y los flags disponibles.

```
usage: polenet.py [-h] -d XML [XML ...] [-w WEIGHTS] [-i [IMAGES [IMAGES ...]]] [-o PATH] [-C CLASS [CLASS ...]] [-S SHAPE] [--logs /path/to/logs/] [-r RATIO] [-s] [-n NUM] <command>

...

positional arguments:
  <command>             Action to perform. Options: train, val, detect, show, visualize

optional arguments:
  -h, --help            show this help message and exit
  -d XML [XML ...], --datasets XML [XML ...]
                        One or more XML files with the description of a Polenet dataset.
  -w WEIGHTS, --weights WEIGHTS
                        Trained or pretrained weights for the Network
  -i [IMAGES [IMAGES ...]], --input [IMAGES [IMAGES ...]]
                        Images to detect
  -o PATH, --output PATH
                        Output directory for the detected images
  -C CLASS [CLASS ...], --classes CLASS [CLASS ...]
                        Polenet classes (ID) to be used in the model.
  -S SHAPE, --mask-shape SHAPE
                        Shape of the mask extrapolated from the bounding box. Options: square, circle, ins_oct, cir_oct
  --logs /path/to/logs/
                        Logs and checkpoints directory (default=logs/)
  -r RATIO, --train-test-ratio RATIO
                        Ratio of images provided to the training and testing sets, where:
                         - num_images * RATIO images are assigned to "train"
                         - num_images * (1-RATIO)  images are assigned to "test"
                        RATIO is expected in the range [0, 1]. Default: 0.9.
  -s, --show-sorted     If specified, while using command "show", the class labels will be orderer by ocurrence.
```

### 4.1 Mostrar clases del dataset

Muestra una lista de todas las clases etiquetadas en el dataset, su nombre y el número de ocurrencias de esa clase en el dataset. Esta acción admite los siguientes flags:

* `-d`|`--datasets`: Lista de XML que componen el dataset. Obligatorio.
* `-s`|`--show-sorted`: Ordenar la lista de clases en orden de ocurrencia en lugar de por ID.

```sh
python polenet.py show -s -d "../PROJ/Tests/22009 Azh/22009 Azh.xml" "../PROJ/Tests/Azahar 21334/Azahar 21334.xml" "../PROJ/Tests/Azahar_21181/Azahar_21181.xml"
```

### 4.2 Visualizar imágenes del dataset con etiquetas

Genera una copia de las imágenes añadiendo los Bounding Box de las clases sobre ellas. Esta acción admite los siguientes flags:

* `-d`|`--datasets`: Lista de XML que componen el dataset. Obligatorio.
* `-i`|`--input`: Lista de imágenes que procesar. Las imágenes deben de formar parte del dataset proporcionado. Obligatorio.
* `-o`|`--output`: Directorio en el que guardar las imágenes generadas. Si no se indica, las imágenes solo se muestran.
* `-C`| `--classes`: Lista con el subset de clases a utilizar, de entre las clases del dataset. Las clases se indican por ID. Si no se indica un subset, se usan todas las clases del dataset.

```sh
python polenet.py visualize -d "../PROJ/Tests/22009 Azh/22009 Azh.xml" "../PROJ/Tests/Azahar 21334/Azahar 21334.xml" "../PROJ/Tests/Azahar_21181/Azahar_21181.xml" -C 7 12 17 -i "../PROJ/Tests/22009 Azh/22009_1_jpg_files/22009_1_s3c0x7687-2464y0-2056m0.jpg" -o ../PROJ/results
```

### 4.3 Entrenar la red

Entrena la red neuronal en el dataset proporcionado. Esta acción admite los siguientes flags:

* `-d`|`--datasets`: Lista de XML que componen el dataset. Obligatorio.
* `-w`|`--weights`: Pesos pre-entrenados que usar de base. Si no se facilitan unos pesos, se utilizará los de COCO por defecto.
* `-C`| `--classes`: Lista con el subset de clases a utilizar, de entre las clases del dataset. Las clases se indican por ID. Si no se indica un subset, se usan todas las clases del dataset.
* `-s`|`--mask-shape`: Forma de la máscara a auto-generar. El dataset por defecto solo contiene los Bounding Box de las clases etiquetadas, no información de las máscaras. La aplicación permite generar de forma automática una máscara basada en el Bounding Box para intentar acercarse más a la forma de los objetos. Las opciones disponibles son:

   * `square`: Usa la forma del Bounding Box como máscara. Valor por defecto.
   * `circle`: Usa como máscara un círculo inscrito en el Bounding Box.
   * `ins_oct`: Usa como máscara un octógono inscrito en el círculo inscrito, con 4 de los vértices en los lados del Bounding Box.
   * `cir_oct`: Usa como máscara un octógono circunscrito en el círculo inscrito, con 4 de los lados sobre los lados del Bounding Box.

* `--logs`: Directorio sobre el que guardar los logs de la ejecución durante el entrenamiento. En este directorio se guarda también los pesos generados a lo largo del entrenamiento. Si no se especifica, el directorio de logs es `./logs`.
* `-r`|`--train-test-ratio`: El ratio de las imágenes del dataset destinadas a entrenamiento o a validación. Las imágenes destinadas al entrenamiento serán las primeras `RATIO * <num_imagenes>` de cada XML. Por defecto, `RATIO` es `0.9`.

```sh
python polenet.py train -d "../PROJ/Tests/22009 Azh/22009 Azh.xml" "../PROJ/Tests/Azahar 21334/Azahar 21334.xml" "../PROJ/Tests/Azahar_21181/Azahar_21181.xml" -C 7 12 17
```

### 4.4 Evaluar la red

Evalua la red entrenada y genera la matriz de confusión. Esta acción admite los siguientes flags:

* `-d`|`--datasets`: Lista de XML que componen el dataset. Obligatorio.
* `-w`|`--weights`: Pesos entrenados de la red. Si no se facilita, el programa intenta encontrar los últimos entrenados en el directorio de logs.
* `-o`|`--output`: Directorio en el que guardar la matriz de confusión. Si no se indica, solo se muestra la imagen.
* `-C`| `--classes`: Lista con el subset de clases a utilizar, de entre las clases del dataset. Las clases se indican por ID. Si no se indica un subset, se usan todas las clases del dataset. Debe coincidir con las clases usadas para el entrenamiento.
* `--logs`: Directorio sobre el que guardar los logs de la ejecución durante la validación. Si no se especifica, el directorio de logs es `./logs`.
* `-r`|`--train-test-ratio`: El ratio de las imágenes del dataset destinadas a entrenamiento o a validación. Las imágenes destinadas a la validación serán las últimas `(1-RATIO) * <num_imagenes>` de cada XML. Por defecto, `RATIO` es `0.9`.

```sh
python polenet.py val -d "../PROJ/Tests/22009 Azh/22009 Azh.xml" "../PROJ/Tests/Azahar 21334/Azahar 21334.xml" "../PROJ/Tests/Azahar_21181/Azahar_21181.xml" -C 7 12 17 -w mask_rcnn_polenet_0030.h5 -o ../PROJ/results
```

### 4.5 Detectar objetos

Detecta los objetos en las imágenes proporcionadas mediante la red entrenada. Esta acción admite los siguientes flags:

* `-d`|`--datasets`: Lista de XML que componen el dataset. Obligatorio. (Requerido para encontrar los nombres de las clases).
* `-w`|`--weights`: Pesos entrenados de la red. Si no se facilita, el programa intenta encontrar los últimos entrenados en el directorio de logs.
* `-i`|`--input`: Lista de imágenes que procesar. Obligatorio.
* `-o`|`--output`: Directorio en el que guardar las imágenes generadas. Si no se indica, las imágenes solo se muestran.
* `-C`| `--classes`: Lista con el subset de clases a utilizar, de entre las clases del dataset. Las clases se indican por ID. Si no se indica un subset, se usan todas las clases del dataset. Debe coincidir con las clases usadas para el entrenamiento.
* `--logs`: Directorio sobre el que guardar los logs de la ejecución durante el entrenamiento. En este directorio se guarda también los pesos generados a lo largo del entrenamiento. Si no se especifica, el directorio de logs es `./logs`.

```sh
python polenet.py detect -d "../PROJ/Tests/22009 Azh/22009 Azh.xml" "../PROJ/Tests/Azahar 21334/Azahar 21334.xml" "../PROJ/Tests/Azahar_21181/Azahar_21181.xml" -C 7 12 17 -w mask_rcnn_polenet_0030.h5 -i "../PROJ/Tests/22009 Azh/22009_1_jpg_files/22009_1_s3c0x7687-2464y0-2056m0.jpg" -o ../PROJ/results
```
