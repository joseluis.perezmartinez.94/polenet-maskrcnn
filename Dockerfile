# Start FROM Nvidia Tensorflow image https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorflow
FROM nvcr.io/nvidia/tensorflow:22.01-tf1-py3

# Install python dependencies                                                                                                        
COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install --no-cache -r requirements.txt

# Create working directory                                                                                                           
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy contents
COPY Mask_RCNN /usr/src/app/Mask_RCNN
COPY polenet /usr/src/app/polenet
