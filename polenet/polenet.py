"""Polenet Dataset Manager for Mask-RCNN

    * Author:  José Luis Pérez Martínez
    * Company: Universitat Politècnica de València
    * Date:    27/02/2022

Copyright (c) 2018 Matterport, Inc.
Licensed under the MIT License (see LICENSE for details)
Based on Waleed Abdulla's Balloons project

This script offers a set of functionalities for managing the Polenet
dataset for working with Mask-RCNN.

This script requires the libraries `numpy`, `skimage`, `cv2` and
`pandas` installed within the environment you are running this script
in.  It also requires the `confmat` module provided with the repository
located in the same directory.

This file can also be imported as a module and contains the following:

Classes
-------
    * PolenetConfig - Configuration of the CNN
    * PolenetDataset - Polenet Dataset for Mask-RCNN

Functions
---------
    * getData - returns the summary of the dataset's labelled classes
    * showData - prints a summary of the dataset's labelled classes
    * visualize - generates the images with the ground truth
    * train - trains the network
    * detect - detects the objects in the provided images
    * evaluate - evaluates the quality of the network
    * main - the main function of the script
"""

import os
import sys
import numpy as np
import skimage.draw
import argparse
import xml.etree.ElementTree as ET
from math import sin, cos, pi
import copy
import cv2
import pandas as pd
import confmat as cm
from imgaug import augmenters as iaa
import matplotlib.pyplot as plot
import tensorflow as tf
from tensorflow import keras

import warnings

# Root directory of the project
CWD = os.getcwd()
ROOT_DIR = os.path.abspath("../")
MASK_RCNN_DIR = os.path.join(ROOT_DIR, 'Mask_RCNN')

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

# Import Mask RCNN
sys.path.append(MASK_RCNN_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils
from mrcnn import visualize

# Path to trained weights file
COCO_WEIGHTS_PATH = os.path.join(MASK_RCNN_DIR, "mask_rcnn_coco.h5")

# Directory to save logs and model checkpoints, if not provided
# through the command line argument --logs
DEFAULT_LOGS_DIR = os.path.join(CWD, "logs")

warnings.filterwarnings("ignore")


############################################################
#  Configurations
############################################################

class PolenetConfig(Config):
    """Configuration for training on the Polenet  dataset.
    
    Derives from the base Config class and overrides some values.
    """
    
    def __init__(self, num_classes, name="polenet", images_per_gpu=1, steps_per_epoch=100, detection_min_confidence=0.9): 
        # Give the configuration a recognizable name
        self.NAME = name

        # We use a GPU with 12GB memory, which can fit two images.
        # Adjust down if you use a smaller GPU.
        self.IMAGES_PER_GPU = images_per_gpu
        
            # Number of classes (including background)
        self.NUM_CLASSES = 1 + num_classes  # Background + pollen

        # Number of training steps per epoch
        self.STEPS_PER_EPOCH = steps_per_epoch

        # Skip detections with < 90% confidence
        self.DETECTION_MIN_CONFIDENCE = detection_min_confidence
        
        self.LOSS_WEIGHTS = {'rpn_class_loss': 1.0, 'rpn_bbox_loss': 0.3, 'mrcnn_class_loss': 1.0, 'mrcnn_bbox_loss': 0.3, 'mrcnn_mask_loss': 0.3}
        
        super().__init__()


############################################################
#  Dataset
############################################################

class PolenetDataset(utils.Dataset):
    """Polenet dataset for training and validation 
    
    Derives from the base utils.Dataset class
    
    Attributes
    ----------
    xml_trees : list
        A list of the loaded XML files with the information of the
        Polenet dataset
    pollen_collection : dict
        A dictionary of classes IDs (keys) and classes names (values)
    pollen_count : dict
        A dictionary of classes IDs (keys) and count of its occurrences
        within the dataset (values)
    
    Methods
    -------
    showData(sort=False)
        Prints the summary of the labelled classes of the dataset.
    load_polenet(subset, subset_ratio=0.9, classes=None, shape='square')
        Loads a subset of the Polenet dataset.
    load_mask(image_id)
        Generates instance masks for an image
    image_reference(image_id)
        Returns the path of an image
    """
    
    def __init__(self, xml_list):
        self.xml_trees = list()
        self.pollen_collection = dict()
        self.pollen_count = dict()

        for xml in xml_list:
            tree = ET.parse(xml)
            self.xml_trees.append((xml, tree))
            for slide in tree.getroot().iter('microscopeSlides'):
                for honey in slide.iter('honeyImages'):
                    for pollen in honey.iter('pollenSamples'):
                        pollen_id = pollen.find('pollenID').text
                        pollen_name = pollen.find('pollenName').text
                        self.pollen_collection[int(pollen_id)] = pollen_name.strip()
                        self.pollen_count[int(pollen_id)] = self.pollen_count.setdefault(int(pollen_id), 0) + 1
                        
        super().__init__()

    def showData(self, sort=False):
        """Prints the summary of the labelled classes of the dataset
    
        Parameters
        ----------
        sort : bool, optional
            A flag used to indicate wether the printed list of classes
            should be orderer by class id (False) of by number of
            ocurrences (True). Default is False.
        
        Returns
        -------
        """
        
        if sort:
            for k, v in sorted(self.pollen_count.items(), key=lambda x: x[1], reverse=True):
                print('[{id:3}] {name}: {count}'.format(name = self.pollen_collection[k], id = k, count = v))
        else:
            for k, v in sorted(self.pollen_count.items()):
                print('[{id:3}] {name}: {count}'.format(name = self.pollen_collection[k], id = k, count = v))
    
    def _square_mask(x, y, size):
        # Vertices of a square of center (`x`, `y`) and side `size` inscribed in the bounding box
        all_points_x = [x+size/2, x-size/2, x-size/2, x+size/2]
        all_points_y = [y+size/2, y+size/2, y-size/2, y-size/2]
        
        return {'all_points_x': all_points_x, 'all_points_y': all_points_y, 'name': 'polygon'}

    def _ins_oct_mask(x, y, size):
        # Vertices of an octagon of center (`x`, `y`) and circumradius `size`/2 inscribed in the bounding box
        all_points_x = list()
        all_points_y = list()
        s = size/2
        
        for i in range(8):
            all_points_x.append(x + s * cos(i*pi/4))
            all_points_y.append(y + s * sin(i*pi/4))
        
        return {'all_points_x': all_points_x, 'all_points_y': all_points_y, 'name': 'polygon'}

    def _cir_oct_mask(x, y, size):
        # Vertices of an octagon of center (`x`, `y`) and apothem `size`/2 inscribed in the bounding box
        all_points_x = list()
        all_points_y = list()
        s = size/2
        
        for i in range(8):
            all_points_x.append(x + s * cos(i*pi/4 + pi/8) / cos(pi/8))
            all_points_y.append(y + s * sin(i*pi/4 + pi/8) / cos(pi/8))
        
        return {'all_points_x': all_points_x, 'all_points_y': all_points_y, 'name': 'polygon'}
    
    def _circle_mask(x, y, size):
        # Circunference of center (`x`, `y`) and diameter `size` inscribed in the bounding box
        return {'cx': x, 'cy': y, 'r': size/2, 'name': 'circle'}
    
    def load_polenet(self, subset, subset_ratio=0.9, classes=None, shape='square', image_w=2464, image_h=2056):
        """Loads a subset of the Polenet dataset
        
        Parameters
        ----------
        subset : {'train', 'val'}
            Subset to load.
        subset_ratio : float [0, 1], optional
            Ratio of the dataset. Default is 0.9. The images assigned
            to each group are determied as:
            * train: the first n images of each microscope slide, where:
                n = `ratio` * <num_microscope_slide_images>
            * val: the last m images of each microscope slide, where:
                m = (1 - `ratio`) * <num_microscope_slide_images>
        classes : list, optional
            List of IDs of the classes to be included in the model,
            either for training, validation or detection. If no classes
            are specified, the model will include all the classes found
            in the dataset.
        shape : {'square', 'circle', 'ins_oct' and 'cir_oct'}, optional
            Shape of the mask that has to be generated from the
            bounding box.
        image_w : int, optional
            Width of the dataset images. Default is 2464.
        image_h : int, optional
            Height of the dataset images. Default is 2056.
        
        Returns
        -------
        """
        
        # Train or validation dataset?
        assert subset in ['train', 'val']
        self.train_subset = list()
        self.val_subset = list()
        
        # Mask shape
        assert shape in ['square', 'circle', 'ins_oct', 'cir_oct']
        masker = None
        if shape == 'circle':
            masker = PolenetDataset._circle_mask
        if shape == 'ins_oct':
            masker = PolenetDataset._ins_oct_mask
        elif shape == 'cir_oct':
            masker = PolenetDataset._cir_oct_mask
        else:
            masker = PolenetDataset._square_mask
        
        class_map = dict()
        if classes:
            for i, v in enumerate(sorted(classes)):
                # Add classes.
                self.add_class("object", i+1, self.pollen_collection[v])
                class_map[v] = i+1
        else:
            for k, v in self.pollen_collection.items():
                # Add classes.
                self.add_class("object", k, v)

        # Add Images
        for xml, tree in self.xml_trees:
            for slide in tree.getroot().iter('microscopeSlides'):
                img_path = os.path.join(os.path.dirname(xml), slide.find('slideName').text)
                for honey in slide.iter('honeyImages'):
                    polygons = list()
                    num_ids = list()
                    for pollen in honey.iter('pollenSamples'):
                        pollen_id = int(pollen.find('pollenID').text)
                        if classes:
                            if pollen_id in classes:
                                pollen_x = float(pollen.find('X').text)
                                pollen_y = float(pollen.find('Y').text)
                                pollen_size = float(pollen.find('pollenSize').text)
                                mask = masker(pollen_x, pollen_y, pollen_size)
                                polygons.append(mask)
                                num_ids.append(class_map[pollen_id])
                        else:
                            pollen_x = float(pollen.find('X').text)
                            pollen_y = float(pollen.find('Y').text)
                            pollen_size = float(pollen.find('pollenSize').text)
                            mask = masker(pollen_x, pollen_y, pollen_size)
                            polygons.append(mask)
                            num_ids.append(pollen_id)
                            
                    if len(num_ids) > 0:
                        img = honey.find('imageName').text
                        len_train = len(self.train_subset)
                        len_val = len(self.val_subset)
                        if len_train == 0 or len_train / (len_train + len_val) < subset_ratio:
                            self.train_subset.append({
                                'object': 'object', 
                                'image_id': img,   # use file name as a unique image id
                                'path': os.path.join(img_path, img), 
                                'width': image_w,
                                'height': image_h,
                                'polygons': polygons,
                                'num_ids': num_ids
                                })
                        else:
                            self.val_subset.append({
                                'object': 'object', 
                                'image_id': img,   # use file name as a unique image id
                                'path': os.path.join(img_path, img), 
                                'width': image_w,
                                'height': image_h,
                                'polygons': polygons,
                                'num_ids': num_ids
                                })
                            
        if subset == 'train':
            self._load_train()
        else:
            self._load_val()

    def _load_train(self):
        for t in self.train_subset:
            self.add_image(
                            t['object'],
                            image_id=t['image_id'],
                            path=t['path'],
                            width=t['width'], height=t['height'],
                            polygons=t['polygons'],
                            num_ids=t['num_ids']
                            )
        
    def _load_val(self):
        for t in self.val_subset:
            self.add_image(
                            t['object'],
                            image_id=t['image_id'],
                            path=t['path'],
                            width=t['width'], height=t['height'],
                            polygons=t['polygons'],
                            num_ids=t['num_ids']
                            )

    def load_mask(self, image_id):
        """Generates instance masks for an image
        
        Parameters
        ----------
        image_id : int
            ID of one of the images loaded into the dataset
        
        Returns
        -------
        masks : list
            A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids : list
            A 1D array of class IDs of the instance masks.
        """
        
        # If not a Polenet dataset image, delegate to parent class.
        image_info = self.image_info[image_id]
        if image_info['source'] != 'object':
            return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        info = self.image_info[image_id]
        if info['source'] != 'object':
            return super(self.__class__, self).load_mask(image_id)
        num_ids = info['num_ids']
        mask = np.zeros([info['height'], info['width'], len(info['polygons'])],
                        dtype=np.uint8)
        for i, p in enumerate(info['polygons']):
            # Get indexes of pixels inside the polygon and set them to 1
            if p['name'] == 'circle':
                rr, cc = skimage.draw.circle(p['cx'], p['cy'], p['r'])
            else:
                rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            
            rr = np.array(list(map(lambda x: min(max(x, 0), info['height']-1), rr)), dtype=np.uint16)
            cc = np.array(list(map(lambda x: min(max(x, 0), info['width']-1), cc)), dtype=np.uint16)
            mask[rr, cc, i] = 1

        # Return mask, and array of class IDs of each instance.
        # Map class names to class IDs.
        num_ids = np.array(num_ids, dtype=np.int32)
        return mask, num_ids #np.ones([mask.shape[-1]], dtype=np.int32)

    def image_reference(self, image_id):
        """Returns the path of the image
        
        Parameters
        ----------
        image_id : int
            ID of one of the images loaded into the dataset
        
        Returns
        -------
        str
            Path of the image
        """
        info = self.image_info[image_id]
        if info['source'] == 'object':
            return info['path']
        else:
            super(self.__class__, self).image_reference(image_id)

############################################################
#  Callback
############################################################

class PolenetCallback(keras.callbacks.Callback):
    """Polenet Callback for training
    
    Derives from the base keras.callbacks.Callback class
    
    Generates logs during the training process.
    
    Attributes
    ----------
        epochs : list
            List of epochs of the current training.
        loss : list
            List of training losses of the current training.
        mrcnn_class_loss : list
            List of training class losses of the current training.
        mrcnn_bbox_loss : list
            List of training bounding box losses of the current
            training.
        mrcnn_mask_loss : list
            List of training mask losses of the the current training.
        val_loss : list
            List of validation losses of the current training.
        val_mrcnn_class_loss : list
            List of validation class losses of the current training.
        val_mrcnn_bbox_loss : list
            List of validation bounding box losses of the current
            training.
        val_mrcnn_mask_loss : list
            List of validation mask losses of the current training.
        log_dir : str
            Directory where the training logs are saved.
        save_fig : bool
            Flag for saving the evaluation figure at the end of the
            training.
        show_fig : bool
            Flag for printing the evaluation figure at the end of the
            training.
    
    Methods
    -------
    on_train_begin(logs=None)
        Called at the begining of the training process.
    on_train_end(logs=None)
        Called at the end of the training process.
    on_epoch_end(epoch, logs=None)
        Called at the end of each epoch.
    """
    
    def __init__(self, log_dir, save_fig=True, show_fig=True):
        self.epochs = list()
        
        self.loss = list()
        self.mrcnn_class_loss = list()
        self.mrcnn_bbox_loss = list()
        self.mrcnn_mask_loss = list()
        
        self.val_loss = list()
        self.val_mrcnn_class_loss = list()
        self.val_mrcnn_bbox_loss = list()
        self.val_mrcnn_mask_loss = list()
        
        self.log_dir = log_dir
        self.save_fig = save_fig
        self.show_fig = show_fig
                        
        super().__init__()
    
    def on_train_begin(self, logs=None):
        """Called at the begining of the training process
        
        Initializes the logged events and creates the logs file.
        """
        
        self.epochs = list()
        
        self.loss = list()
        self.mrcnn_class_loss = list()
        self.mrcnn_bbox_loss = list()
        self.mrcnn_mask_loss = list()
        
        self.val_loss = list()
        self.val_mrcnn_class_loss = list()
        self.val_mrcnn_bbox_loss = list()
        self.val_mrcnn_mask_loss = list()
        
        # Create log_dir if it does not exist
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)
        with open(os.path.join(self.log_dir, 'logs.txt'), 'w') as f:
            f.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format('epoch', 'loss', 'class_loss', 'bbox_loss', 'mask_loss', 'val_loss', 'val_class_loss', 'val_bbox_loss', 'val_mask_loss'))

    def on_train_end(self, logs=None):
        """Called at the end of the training process
        
        Generates different figures with the events captured during the
        training process.
        
        With `save_fig` flag saves the generated figures.
        
        With `show_fig` flag shows the figures on the screen.
        """
        
        # make a new figure
        plot.figure(figsize=(40,20))
        
        # loss
        plot.subplot(241)
        plot.plot(self.epochs, self.loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Train Loss')
        plot.grid(True)
        
        # class_loss  
        plot.subplot(242)
        plot.plot(self.epochs, self.mrcnn_class_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Train Class Loss')
        plot.grid(True)
        
        # bbox_loss  
        plot.subplot(243)
        plot.plot(self.epochs, self.mrcnn_bbox_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Train BBox Loss')
        plot.grid(True)
        
        # mask_loss  
        plot.subplot(244)
        plot.plot(self.epochs, self.mrcnn_mask_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Train Mask Loss')
        plot.grid(True)
        
        # val_loss
        plot.subplot(245)
        plot.plot(self.epochs, self.val_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Val Loss')
        plot.grid(True)
        
        # val_class_loss
        plot.subplot(246)
        plot.plot(self.epochs, self.val_mrcnn_class_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Val Class Loss')
        plot.grid(True)
        
        # val_bbox_loss
        plot.subplot(247)
        plot.plot(self.epochs, self.val_mrcnn_bbox_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Val BBox Loss')
        plot.grid(True)
        
        # val_mask_loss
        plot.subplot(248)
        plot.plot(self.epochs, self.val_mrcnn_mask_loss, 'b-')
        plot.xlabel('Epoch')
        plot.ylabel('Loss')
        plot.title('Val Mask Loss')
        plot.grid(True)
        
        if self.save_fig:
            plot.savefig(os.path.join(self.log_dir, 'results.png'))
        if self.show_fig:
            plot.show()

    def on_epoch_begin(self, epoch, logs=None):
        pass

    def on_epoch_end(self, epoch, logs=None):
        """Called at the end of each epoch
        
        Stores the evaluation events of the current epoch internally
        and into the logs file.
        """
        
        self.epochs.append(epoch)
        
        self.loss.append(logs['loss'])
        self.mrcnn_class_loss.append(logs['mrcnn_class_loss'])
        self.mrcnn_bbox_loss.append(logs['mrcnn_bbox_loss'])
        self.mrcnn_mask_loss.append(logs['mrcnn_mask_loss'])
        
        self.val_loss.append(logs['val_loss'])
        self.val_mrcnn_class_loss.append(logs['val_mrcnn_class_loss'])
        self.val_mrcnn_bbox_loss.append(logs['val_mrcnn_bbox_loss'])
        self.val_mrcnn_mask_loss.append(logs['val_mrcnn_mask_loss'])
        
        with open(os.path.join(self.log_dir, 'logs.txt'), 'a') as f:
            f.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(epoch, 
                                                                  logs['loss'],
                                                                  logs['mrcnn_class_loss'],
                                                                  logs['mrcnn_bbox_loss'],
                                                                  logs['mrcnn_mask_loss'],
                                                                  logs['val_loss'],
                                                                  logs['val_mrcnn_class_loss'],
                                                                  logs['val_mrcnn_bbox_loss'],
                                                                  logs['val_mrcnn_mask_loss']))

    def on_test_begin(self, logs=None):
        pass

    def on_test_end(self, logs=None):
        pass

    def on_predict_begin(self, logs=None):
        pass

    def on_predict_end(self, logs=None):
        pass

    def on_train_batch_begin(self, batch, logs=None):
        pass

    def on_train_batch_end(self, batch, logs=None):
        pass

    def on_test_batch_begin(self, batch, logs=None):
        pass

    def on_test_batch_end(self, batch, logs=None):
        pass

    def on_predict_batch_begin(self, batch, logs=None):
        pass

    def on_predict_batch_end(self, batch, logs=None):
        pass


############################################################
#  Visualize
############################################################

def visualize(ref_dataset, config, images, save_dir, classes, shape='square'):
    """Generates a copy of the images with the ground truth
    
    Parameters
    ----------
    ref_dataset : obj
        Instance of PolenetDataset with the current dataset.
    config : obj
        Instance of PolenetConfig with the appropriate configuration
        for inference.
    images : list
        List of the path to the images to visualize. The provided
        images should be within the dataset.
    classes : list
        List of IDs of the classes to be included in the model.
    save_dir : str
        Path to the directory where the generated images should be
        saved.
    shape : {'square', 'circle', 'ins_oct' and 'cir_oct'}, optional
        Shape of the mask that has to be generated from the
        bounding box.
    
    Returns
    -------
    """
    
    # Dataset
    dataset = copy.deepcopy(ref_dataset)
    dataset.load_polenet('train', subset_ratio=1.0, classes=classes, shape=shape)
    dataset.prepare()


    if classes is None:
        classes = sorted(list(dataset.pollen_collection.keys()))
    num_classes = len(classes)

    class_names = [dataset.pollen_collection[k] for k in sorted(classes)]
    dataset_images = [im['id'] for im in dataset.image_info]

    for im in images:
        image_name = os.path.basename(im)

        if image_name in dataset_images:
            image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset, config, dataset_images.index(image_name), use_mini_mask=False)

            display_results(cv2.cvtColor(image, cv2.COLOR_RGB2BGR), gt_bbox, gt_mask, gt_class_id, class_names, save_dir=save_dir, img_name='gt_' + image_name, display_img=False)
        else:
            print('Image {} was not found in the provided dataset!'.format(image_name))
            

############################################################
#  Train
############################################################

def train(model, config, ref_dataset, classes, subset_ratio=0.9, shape='square', epochs=30, logs=None):
    """Trains the model
    
    Parameters
    ----------
    model : obj
        Mask-RCNN model configured for training and with the
        pre-trained weights loaded.
    config : obj
        Instance of PolenetConfig with the appropriate configuration
        for training.
    ref_dataset : obj
        Instance of PolenetDataset with the current dataset
    classes : list
        List of IDs of the classes to be included in the model.
    subset_ratio : float [0, 1], optional
        Ratio of the dataset. Default is 0.9. The images assigned
        to each group are determied as:
        * train: the first n images of each microscope slide, where:
            n = `ratio` * <num_microscope_slide_images>
        * val: the last m images of each microscope slide, where:
            m = (1 - `ratio`) * <num_microscope_slide_images>
    shape : {'square', 'circle', 'ins_oct' and 'cir_oct'}, optional
        Shape of the mask that has to be generated from the
        bounding box.
    epochs : int, optional
        Number of training epochs. Default is 30.
    logs : str, optional
        Path to the logs directory. Default is Mask-RCNN model
        model_dir (`model.model_dir`).
    
    Returns
    -------
    """
    
    # Training dataset.
    dataset_train = copy.deepcopy(ref_dataset)
    dataset_train.load_polenet('train', subset_ratio=subset_ratio, classes=classes, shape=shape)
    dataset_train.prepare()

    # Validation dataset
    dataset_val = copy.deepcopy(ref_dataset)
    dataset_val.load_polenet('val', subset_ratio=subset_ratio, classes=classes, shape=shape)
    dataset_val.prepare()
    
    if not logs:
        logs = model.model_dir
    
    # Añadir data augmentation como describe en
    # https://github.com/matterport/Mask_RCNN/issues/1924
    max_augs = 3
    augmentation = iaa.SomeOf((0, max_augs), [
        # horizontally flip 50% of the images
        iaa.Fliplr(0.5),
        # vertically flip 50% of the images
        iaa.Flipud(0.5),
        # random crops
        #iaa.Crop(percent=(0, 0.1)),   
        # Apply affine transformations to each image.
        # Scale/zoom them, translate/move them, rotate them and shear them.
        iaa.Affine(
            scale={"x": (0.95, 1.05), "y": (0.95, 1.05)},
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
            rotate=(-10, 10),
            shear=(-4, 4)
        ),
        # Add a value of -10 to 10 to each pixel.
        #iaa.Add((-10, 10)),
        # Make some images brighter and some darker.
        #iaa.Multiply((0.8, 1.2)),
        # blur images with a sigma of 0 to 0.5
        # But we only blur about 20% of all images.
        iaa.Sometimes(
            0.2,
            iaa.GaussianBlur(sigma=(0, 0.5))
        )
    ])
    
    # Callback
    polenet_callback = PolenetCallback(model.model_dir, save_fig=True, show_fig=True)
    
    # *** This training schedule is an example. Update to your needs ***
    # Since we're using a very small dataset, and starting from
    # COCO trained weights, we don't need to train too long.
    # We train 'all' layers
    print("Training network")
    
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=epochs,
                augmentation=augmentation,
                custom_callbacks=[polenet_callback],
                #layers='heads')
                layers='all')


def color_splash(image, mask):
    #DEPRECATED: Not used in this application
    
    """Apply color splash effect.
    image: RGB image [height, width, 3]
    mask: instance segmentation mask [height, width, instance count]

    Returns result image.
    """
    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
    # Copy color pixels from the original color image where mask is set
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        splash = np.where(mask, image, gray).astype(np.uint8)
    else:
        splash = gray.astype(np.uint8)
    return splash


def detect_and_color_splash(model, image_path=None, video_path=None):
    #DEPRECATED: Not used in this application
    
    assert image_path or video_path

    # Image or video?
    if image_path:
        # Run model detection and generate the color splash effect
        print("Running on {}".format(args.image))
        # Read image
        image = skimage.io.imread(args.image)
        # Detect objects
        r = model.detect([image], verbose=1)[0]
        # Color splash
        splash = color_splash(image, r['masks'])
        # Save output
        file_name = "splash_{:%Y%m%dT%H%M%S}.png".format(datetime.datetime.now())
        skimage.io.imsave(file_name, splash)
    elif video_path:
        # Video capture
        vcapture = cv2.VideoCapture(video_path)
        width = int(vcapture.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vcapture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = vcapture.get(cv2.CAP_PROP_FPS)

        # Define codec and create video writer
        file_name = "splash_{:%Y%m%dT%H%M%S}.avi".format(datetime.datetime.now())
        vwriter = cv2.VideoWriter(file_name,
                                  cv2.VideoWriter_fourcc(*'MJPG'),
                                  fps, (width, height))

        count = 0
        success = True
        while success:
            print("frame: ", count)
            # Read next image
            success, image = vcapture.read()
            if success:
                # OpenCV returns images as BGR, convert to RGB
                image = image[..., ::-1]
                # Detect objects
                r = model.detect([image], verbose=0)[0]
                # Color splash
                splash = color_splash(image, r['masks'])
                # RGB -> BGR to save image to video
                splash = splash[..., ::-1]
                # Add image to video writer
                vwriter.write(splash)
                count += 1
        vwriter.release()
    print("Saved to ", file_name)


############################################################
#  Show
############################################################

def getData(xml_list):
    """Gets the summary of the labelled classes of the dataset
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset
    
    Returns
    -------
    pollen_collection : dict
        Dictionary of class ids (key) and class names (value) found at
        the dataset
    pollen_count : dict
        Dictionary of class ids (key) and its number of ocurrences
        (value) within the dataset
    """
    
    pollen_collection = dict()
    pollen_count = dict()

    # Check every XML for pollen labelled classes
    for xml in xml_list:
        tree_1 = ET.parse(xml)
        # Each XML may have several microscope slides
        for slide in tree_1.getroot().iter('microscopeSlides'):
            # Each microscope slide has several honey images
            for honey in slide.iter('honeyImages'):
                # Each honey image has some pollen samples labelled
                for pollen in honey.iter('pollenSamples'):
                    pollen_id = pollen.find('pollenID').text
                    pollen_name = pollen.find('pollenName').text
                    pollen_collection[int(pollen_id)] = pollen_name.strip()
                    pollen_count[int(pollen_id)] = pollen_count.setdefault(int(pollen_id), 0) + 1
                    
    return pollen_collection, pollen_count


def showData(xml_list, sort=False):
    """Prints the summary of the labelled classes of the dataset
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset
    sort : bool, optional
        A flag used to indicate wether the printed list of classes
        should be orderer by class id (False) of by number of
        ocurrences (True). Default is False.
    
    Returns
    -------
    """
    
    # Get the data from the dataset
    pollen_collection, pollen_count = getData(xml_list)

    if sort:
        # Sort the classes by the number of ocurrences
        for k, v in sorted(pollen_count.items(), key=lambda x: x[1], reverse=True):
            print('[{id:3}] {name}: {count}'.format(name = pollen_collection[k], id = k, count = v))
    else:
        # Sort the classes by class ID
        for k, v in sorted(pollen_count.items()):
            print('[{id:3}] {name}: {count}'.format(name = pollen_collection[k], id = k, count = v))


############################################################
#  Detect
############################################################

def color_map(N=256, normalized=False):
    """Maps the `N` values to different colors
    
    Reference: https://github.com/matterport/Mask_RCNN/issues/134#issuecomment-424297149
    """
    
    def bitget(byteval, idx):
        return ((byteval & (1 << idx)) != 0)

    dtype = 'float32' if normalized else 'uint8'
    cmap = np.zeros((N, 3), dtype=dtype)
    for i in range(N):
        r = g = b = 0
        c = i
        for j in range(8):
            r = r | (bitget(c, 0) << 7 - j)
            g = g | (bitget(c, 1) << 7 - j)
            b = b | (bitget(c, 2) << 7 - j)
            c = c >> 3

        cmap[i] = np.array([r, g, b])

    cmap = cmap / 255 if normalized else cmap
    return cmap


def display_results(image, boxes, masks, class_ids, class_names, scores=None,
                        show_mask=True, show_bbox=True, display_img=True,
                        save_img=True, save_dir=None, img_name=None):
        """
        boxes: [num_instance, (y1, x1, y2, x2, class_id)] in image coordinates.
        masks: [height, width, num_instances]
        class_ids: [num_instances]
        class_names: list of class names of the dataset (Without Background)
        scores: (optional) confidence scores for each box
        show_mask, show_bbox: To show masks and bounding boxes or not
        display_img: To display the image in popup
        save_img: To save the predict image
        save_dir: If save_img is True, the directory where you want to save the predict image
        img_name: If save_img is True, the name of the predict image
        
        Reference: https://github.com/matterport/Mask_RCNN/issues/134#issuecomment-424297149
        """
        n_instances = boxes.shape[0]
        colors = color_map(len(class_ids))
        for k in range(n_instances):
            color = colors[class_ids[k]].astype(np.int)
            color = (int(color[0]), int(color[1]), int(color[2]))
            if show_bbox:
                box = boxes[k]
                cls = class_names[class_ids[k]-1]  # Skip the Background
                cv2.rectangle(image, (box[1], box[0]), (box[3], box[2]), color, 1)
                font = cv2.FONT_HERSHEY_SIMPLEX
                if scores:
                    score = scores[k]
                    cv2.putText(image, '{}: {:.3f}'.format(cls, score), (box[1], box[0]),
                            font, 1, (0, 255, 255), 1, cv2.LINE_AA)
                else:
                    cv2.putText(image, '{}'.format(cls), (box[1], box[0]),
                            font, 1, (0, 255, 255), 1, cv2.LINE_AA)

            if show_mask:
                mask = masks[:, :, k]
                color_mask = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.int)
                color_mask[mask] = color
                image = cv2.addWeighted(color_mask, 0.5, image.astype(np.int), 1, 0)

        if display_img:
            plt.imshow(image)
            plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
            plt.show()
        if save_img:
            cv2.imwrite(os.path.join(save_dir, img_name), image)
            print('Saved image {0} at {1}'.format(img_name, save_dir))

        return None


def detect(model, config, images, save_dir, classes=None, threshold=0):
    """Detects the objects in the provided images
    
    Parameters
    ----------
    model : obj
        Mask-RCNN model configured for inference and with the
        trained weights loaded.
    config : obj
        Instance of PolenetConfig with the appropriate configuration
        for inference.
    images : list
        List of the path to the images to detect.
    save_dir : str
        Path to the directory where the generated images should be
        saved.
    classes : list
        List of IDs of the classes to be included in the model.
    
    Returns
    -------
    """

    # Detect objects
    image_list = np.array(skimage.io.imread(im) for im in images)
    results = model.detect(image_list, verbose=1)
    
    for i, r in enumerate(results):
        image_name = os.path.basename(images[i])
        display_results(cv2.cvtColor(image_list[i], cv2.COLOR_RGB2BGR), r['rois'], r['masks'], r['class_ids'], classes, scores=r['scores'], save_dir=save_dir, img_name='segmented_' + image_name, display_img=False)
        
        """ DEPRECATED: Not used in this script
        
        # Run model detection and generate the color splash effect
        print("Running on {}".format(images[i]))
        
        # Color splash
        splash = color_splash(image, r['masks'])
        # Save output
        file_name = "splash_{:%Y%m%dT%H%M%S}.png".format(datetime.datetime.now())
        skimage.io.imsave(file_name, splash)
        """


############################################################
#  Eval
############################################################

def load_gt_images(config, dataset):
    images = list()
    for image_id in dataset.image_ids:
        image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset, config, image_id, use_mini_mask=False)
        images.append(image)
    return images

def evaluate(model, config, ref_dataset, classes, subset_ratio=0.9, shape='square', save_dir=None, threshold=0):
    """Evaluates the quality of the network
    
    Parameters
    ----------
    model : obj
        Mask-RCNN model configured for inference and with the
        trained weights loaded.
    config : obj
        Instance of PolenetConfig with the appropriate configuration
        for inference.
    ref_dataset : obj
        Instance of PolenetDataset with the current dataset
    classes : list
        List of IDs of the classes to be included in the model.
    subset_ratio : float [0, 1], optional
        Ratio of the dataset. Default is 0.9. The images assigned
        to each group are determied as:
        * train: the first n images of each microscope slide, where:
            n = `ratio` * <num_microscope_slide_images>
        * val: the last m images of each microscope slide, where:
            m = (1 - `ratio`) * <num_microscope_slide_images>
    shape : {'square', 'circle', 'ins_oct' and 'cir_oct'}, optional
        Shape of the mask that has to be generated from the
        bounding box.
    save_dir : str, optional
        Path to the directory where the generated files should be
        saved. If not provided, the confusion matrix and the output
        json with the evaluation data will not be saved.
    """
    
    # Validation dataset
    dataset_val = copy.deepcopy(ref_dataset)
    dataset_val.load_polenet('val', subset_ratio=subset_ratio, classes=classes, shape=shape)
    dataset_val.prepare()

    #ground-truth and predictions lists
    gt_tot = np.array([])
    pred_tot = np.array([])
    #mAP list
    mAP_ = []
    
    #compute gt_tot, pred_tot and mAP for each image in the test dataset
    for image_id in dataset_val.image_ids:
        image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset_val, config, image_id, use_mini_mask=False)
        info = dataset_val.image_info[image_id]

        results = model.detect([image], verbose=1)
        r = results[0]
        
        for i, score in sorted(enumerate(r['scores']), reverse=True):
            if score < threshold:
                r['rois'].pop(i)
                r['class_ids'].pop(i)
                r['scores'].pop(i)
                r['masks'].pop(i)
        
        #compute gt_tot and pred_tot
        gt, pred = cm.gt_pred_lists(gt_class_id, gt_bbox, r['class_ids'], r['rois'])
        gt_tot = np.append(gt_tot, gt)
        pred_tot = np.append(pred_tot, pred)
        
        #precision_, recall_, AP_ 
        AP_, precision_, recall_, overlap_ = utils.compute_ap(gt_bbox, gt_class_id, gt_mask,
                                            r['rois'], r['class_ids'], r['scores'], r['masks'])
        #check if the vectors len are equal
        print("the actual len of the gt vect is : ", len(gt_tot))
        print("the actual len of the pred vect is : ", len(pred_tot))
        
        mAP_.append(AP_)
        print("Average precision of this image : ",AP_)
        print("The actual mean average precision for the whole images (matterport methode) ", sum(mAP_)/len(mAP_))
        print("Ground truth object : ", gt)
        print("Predicted object : ", pred)
    
    gt_tot=gt_tot.astype(int)
    pred_tot=pred_tot.astype(int)
    #save the vectors of gt and pred
    gt_pred_tot_json = {"gt_tot" : gt_tot, "pred_tot" : pred_tot}
    df = pd.DataFrame(gt_pred_tot_json)
    
    if save_dir:
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        df.to_json(os.path.join(save_dir,"gt_pred_test.json"))
        
    #print the confusion matrix and compute true postives, false positives and false negative for each class: 
    #ps : you can controle the figure size and text format by choosing the right values
    tp, fp, fn = cm.plot_confusion_matrix_from_data(gt_tot, pred_tot, [dataset_val.class_names[c] for c in sorted(np.unique(gt_tot))], fz=18, figsize=(20,20), lw=0.5, save_path=os.path.join(save_dir,"conf_matrix.png"))
    
    print('####################')
    print('True Positives: ', sum(tp))
    print('False Positives:', sum(fp[1:]))
    print('False Negatives:', sum(fn[1:]))
    print('####################')


############################################################
#  Main
############################################################

def main(args):
    """Main function of the script
    
    Parameters
    ----------
    args : list
        List of input parameters and flags
    
    Returns
    -------
    """
    
    # Parse inputs
    parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawTextHelpFormatter)
      
    parser.add_argument(
        'command',
        metavar='<command>',
        choices=['train', 'val', 'detect', 'show', 'visualize'],
        help='Action to perform. Options: train, val, detect, show, visualize')
    parser.add_argument(
        '-d', '--datasets',
        metavar='XML',
        nargs='+',
        required=True,
        help='One or more XML files with the description of a Polenet dataset.')
    parser.add_argument(
        '-w', '--weights',
        metavar='WEIGHTS',
        help='Trained or pretrained weights for the Network')
    parser.add_argument(
        '-i', '--input',
        metavar='IMAGES',
        nargs='*',
        help='Images to detect')
    parser.add_argument(
        '-o', '--output',
        metavar='PATH',
        default='.',
        help='Output directory for the detected images')
    parser.add_argument(
        '-C', '--classes',
        metavar='CLASS',
        nargs='+',
        type=int,
        help='Polenet classes (ID) to be used in the model.')
    parser.add_argument(
        '-S', '--mask-shape',
        metavar='SHAPE',
        default='square',
        choices=['square', 'circle', 'ins_oct', 'cir_oct'],
        help='Shape of the mask extrapolated from the bounding box. Options: square, circle, ins_oct, cir_oct')
    parser.add_argument(
        '--logs',
        metavar='/path/to/logs/',
        required=False,
        default=DEFAULT_LOGS_DIR,
        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument(
        '-r', '--train-test-ratio',
        metavar='RATIO',
        default=0.9,
        type=float,
        help='Ratio of images provided to the training and testing sets, where:\n - num_images * RATIO images are assigned to "train"\n - num_images * (1-RATIO)  images are assigned to "test"\nRATIO is expected in the range [0, 1]. Default: 0.9.')
    parser.add_argument(
        '-s', '--show-sorted',
        action='store_true',
        help='If specified, while using command "show", the class labels will be orderer by ocurrence.')
    parser.add_argument(
        '-t', '--threshold',
        metavar='THRESHOLD',
        type=float,
        default=0,
        help='Precision threshold for the detected objects. Default: 0.')
    parser.add_argument(
        '-e', '--epochs',
        metavar='EPOCHS',
        type=int,
        default=30,
        help='Number of epochs of training. Default: 30.')
    
    args = parser.parse_args()
    
    dataset_list = list()
    for d in args.datasets:
        if d.endswith('.xml'):
            dataset_list.append(d)
        elif d.endswith('.txt'):
            with open(d) as f:
                for line in f:
                    line = line.strip()
                    if line.endswith('.xml'):
                        file_path = os.path.dirname(d)
                        dataset_list.append(os.path.join(file_path, line.strip()))
    
    dataset = PolenetDataset(dataset_list)
    
    if args.classes is None:
        args.classes = sorted(list(dataset.pollen_collection.keys()))
    num_classes = len(args.classes)
    
    # Show dataset info/summary
    if args.command == 'show':
        dataset.showData(sort=args.show_sorted)
    
    # Visualize Ground Truth
    elif args.command == 'visualize':
        assert args.input
        
        # Configurations
        config = PolenetConfig(num_classes)
        config.GPU_COUNT = 1
        config.IMAGES_PER_GPU = 1
        config.display()
        
        visualize(dataset, config, args.input, args.output, classes=args.classes, shape=args.mask_shape)
    
    # Train network
    elif args.command == 'train':
        # Configurations
        config = PolenetConfig(num_classes)
        config.display()
            
        # Create model
        model = modellib.MaskRCNN(mode="training", config=config,
                                        model_dir=args.logs)

        # Select weights file to load
        if not args.weights:
            weights_path = os.path.join(CWD, 'coco.h5')
            # Download weights file
            if not os.path.exists(weights_path):
                utils.download_trained_weights(weights_path)
        else:
            weights_path = args.weights

        model.load_weights(weights_path, by_name=True, exclude=[
                    "mrcnn_class_logits", "mrcnn_bbox_fc",
                    "mrcnn_bbox", "mrcnn_mask"])

        train(model, config, dataset, args.classes, subset_ratio=args.train_test_ratio, shape=args.mask_shape, epochs=args.epochs, logs=args.logs)
    
    # Evaluate network
    elif args.command == 'val':
         # Configurations
        config = PolenetConfig(num_classes)
        config.GPU_COUNT = 1
        config.IMAGES_PER_GPU = 1
        config.display()
        
        # Recreate the model in inference mode
        model = modellib.MaskRCNN(mode="inference", 
                                config=config,
                                model_dir=args.logs)
        
        # Select weights file to load
        if not args.weights:
            weights_path = model.find_last()
        else:
            weights_path = args.weights

        # Load trained weights
        #print("Loading weights from ", model_path)
        model.load_weights(weights_path, by_name=True)
        
        evaluate(model, config, dataset, args.classes, subset_ratio=args.train_test_ratio, shape=args.mask_shape, save_dir=args.output, threshold=args.threshold)
        
    # Detect objects
    elif args.command == 'detect':
        assert args.input
        
        # Configurations
        config = PolenetConfig(num_classes)
        config.GPU_COUNT = 1
        config.IMAGES_PER_GPU = 1
        config.display()

        # Recreate the model in inference mode
        model = modellib.MaskRCNN(mode="inference", 
                                config=config,
                                model_dir=args.logs)
        
        # Select weights file to load
        if not args.weights:
            weights_path = model.find_last()
        else:
            weights_path = args.weights

        # Load trained weights
        #print("Loading weights from ", model_path)
        model.load_weights(weights_path, by_name=True)
        
        class_names = dataset.pollen_collection
        detect(model, config, args.input, args.output, [class_names[k] for k in sorted(args.classes)], threshold=args.threshold)


if __name__ == '__main__':
    main(sys.argv[1:])
